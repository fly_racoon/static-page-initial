const autoprefixer = require('autoprefixer');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');


module.exports = {

  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    open: true,
    port: 9000,
    watchContentBase: true,
  },

  entry: [
    './src/js/index.js',
    './src/styles/main.scss',
  ],

  mode: 'development',

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.scss$/,
        use: [
          MiniCSSExtractPlugin.loader,
          {
            loader: "css-loader",
          },
          {
            loader: "postcss-loader",
            options: {
              plugins:(
                [
                  autoprefixer({
                    browsers: ['ie >= 8', 'last 4 versions']
                  })
                ]
              ),
              sourceMaps: true
            }
          },
          {
            loader: "sass-loader",
            options: {sourceMap: true}
          },
        ],
      }
    ]
  },

  output: {
    filename: 'js/index.js',
    path: path.resolve(__dirname, './dist')
  },

  plugins: [
    new MiniCSSExtractPlugin({
      filename: "./styles/[name].css",
      chunkFilename: "./styles/[id].css"
    }),
    new CopyWebpackPlugin([
      {
        from: './src/images',
        to: './images',
      },
      {
        from: './src/index.html',
        to: './index.html',
      }
    ])
  ]
};